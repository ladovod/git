<?php if (isset($question)): ?>
  <span class="question"><?php print $question; ?></span>
<?php endif; ?>

<?php if (isset($answer1)): ?>
  <span class="answer"><?php print $answer1; ?></span>
<?php endif; ?>
<?php if (isset($answer2)): ?>
  <span class="answer"><?php print $answer2; ?></span>
<?php endif; ?>
<?php if (isset($answer3)): ?>
  <span class="answer"><?php print $answer3; ?></span>
<?php endif; ?>
<?php if (isset($answer4)): ?>
  <span class="answer"><?php print $answer4; ?></span>
<?php endif; ?>

<?php if (isset($right_answer)): ?>
  <span class="answer"><?php print $right_answer; ?></span>
<?php endif; ?>

